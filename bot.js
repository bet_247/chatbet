//
// This is main file containing code implementing the Express server and functionality for the Express echo bot.
//
'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const path = require('path');
var https = require('https');

var config = require("./config");
var betfair = require("/chatbet/config/betfair");

var db = config.database;
var options = betfair.options;

//var test_parse = JSON.parse(test);
console.log("PRETTY:" + JSON.stringify(options, null, 2) ) ;

var messengerButton = "<html><head><title>Facebook Messenger Bot</title></head><body><h1>Facebook Messenger Bot</h1>This is a bot based on Messenger Platform QuickStart. For more details, see their <a href=\"https://developers.facebook.com/docs/messenger-platform/guides/quick-start\">docs</a>.<script src=\"https://button.glitch.me/button.js\" data-style=\"glitch\"></script><div class=\"glitchButton\" style=\"position:fixed;top:20px;right:20px;\"></div></body></html>";

// The rest of the code implements the routes for our Express server.
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
		extended: true
}));

var FIRST_INDEX = 0;
var DEFAULT_ENCODING = 'utf-8';
var DEFAULT_JSON_FORMAT = '\t';

// Webhook validation
app.get('/webhook', function(req, res) {
		console.error("hub.mode=" + req.query['hub.mode'] + ":") ;
		console.error("hub.verify_token=" + req.query['hub.verify_token'] + ":" + process.env.VERIFY_TOKEN + ":" ) ;
		if (req.query['hub.mode'] === 'subscribe' &&
			req.query['hub.verify_token'] === process.env.VERIFY_TOKEN) {
		console.log("Validating webhook");
		res.status(200).send(req.query['hub.challenge']);
			} else {
				console.error("Failed validation. Make sure the validation tokens match.");
				res.sendStatus(403);          
			}
});

// Display the web page
app.get('/', function(req, res) {
		console.log("Display basic webpage");
		res.writeHead(200, {'Content-Type': 'text/html'});
		res.write(messengerButton);
		res.end();
});

// Message processing
app.post('/webhook', function (req, res) {
		console.log(req.body);
		var data = req.body;
		
		
		// Make sure this is a page subscription
		if (data.object === 'page') {
			
			// Iterate over each entry - there may be multiple if batched
			data.entry.forEach(function(entry) {
					var pageID = entry.id;
					var timeOfEvent = entry.time;
					
					// Iterate over each messaging event
					entry.messaging.forEach(function(event) {
							if (event.message) {
								receivedMessage(event);
							} else if (event.postback) {
								receivedPostback(event);   
							} else {
								console.log("Webhook received unknown event: ", event);
							}
					});
			});
			
			
			// Assume all went well.
			//
			// You must send back a 200, within 20 seconds, to let us know
			// you've successfully received the callback. Otherwise, the request
			// will time out and we will keep trying to resend.
			res.sendStatus(200);
		}
});

// Incoming events handling
function receivedMessage(event) {
	var senderID = event.sender.id;
	var recipientID = event.recipient.id;
	var timeOfMessage = event.timestamp;
	var message = event.message;
	
	console.log("Received message for user %d and page %d at %d with message:", 
		senderID, recipientID, timeOfMessage);
	console.log(JSON.stringify(message));
	
	var messageId = message.mid;
	
	var messageText = message.text;
	var messageAttachments = message.attachments;
	
	if (messageText) {
		// If we receive a text message, check to see if it matches a keyword
		// and send back the template example. Otherwise, just echo the text we received.
		var command = messageText.split(" ") ;
		console.log("Split:%s", command[0]);   
		
		// Re-read betfair config to update for session token changes.
		delete require.cache[require.resolve('/chatbet/config/betfair')];
		betfair = require("/chatbet/config/betfair");
		options = betfair.options;
		
		switch (command[0]) {
			
		case '/bet':
			switch (command[1]) {
			case 'welcome':
				sendWelcomeMessage(senderID);
				break;
			case 'help':
				sendHelpMessage(senderID);
				break;
			case 'link':
				sendLinkMessage(senderID);
				break;
			case 'test':
				sendListMessage(senderID);
				break;
			case 'webview':
				sendWebViewMessage(senderID);
				break;
			case 'list':
				listUsers(senderID) ;
				break;
			case 'place':
				getAllEventTypes(senderID, options);
				break;
			default:
				sendTextMessage(senderID, "Unknown Command: " + messageText );
			}
			break;
			
		default:
			sendTextMessage(senderID, messageText);
		}
	} else if (messageAttachments) {
		sendTextMessage(senderID, "Message with attachment received");
	}
}

function receivedPostback(event) {
	console.log("PRETTY:" + JSON.stringify(event, null, 2) ) ;
	
	var senderID = event.sender.id;
	var recipientID = event.recipient.id;
	var timeOfPostback = event.timestamp;
	
	// The 'payload' param is a developer-defined field which is set in a postback 
	// button for Structured Messages. 
	var payload = event.postback.payload;
	
	console.log("Received postback for user %d and page %d with payload '%s' " + 
		"at %d", senderID, recipientID, payload, timeOfPostback);
	var response = JSON.parse(payload);
	console.log("POSTBACK PAYLOAD:" + JSON.stringify(response, null, 2) ) ;
	// When a postback is called, we'll send the welcome message back to the user.
    console.log("response.type:" + response.type) ;		
	switch (response.type) {
		
	case 'GET_STARTED_PAYLOAD':
		sendWelcomeMessage(senderID);
		break ;
	case 'event':
 		var messageText = "Home: " + response.home + " v " + "Away:" + response.away + "\n" ;
		sendTextMessage(senderID, messageText);
		break ;
	default:
		console.log("Payload:" + payload) ;
		break ;
	}
}

//////////////////////////
// Sending helpers
//////////////////////////
function sendTextMessage(recipientId, messageText) {
	var messageData = {
		recipient: {
			id: recipientId
		},
		message: {
			text: messageText
		}
	};
	
	callSendAPI(messageData);
}

function sendWelcomeMessage(recipientId) {
	
	var messageText = `
	Welcome to chatbet!
	
	The world leading in-chat solution to betting!
	
	You are using an invite only beta test solution which is running on the betfair exchange only.
	
	Other restrictions:
	- Limited to Football (Soccer) Competitions
	- Limited list of 5 teams per competition
	- Minimum bet of \u00A32, Maximum of \u00A34
	
	Please feedback any issues, improvements, or general comments straight into the chat.
	For a list of commands you can type /bet help
	
	Lets get you started by linking your betfair account, type /bet link
	` ;
	
	sendTextMessage(recipientId, messageText) ;
}



function sendHelpMessage(recipientId) {
	
	var messageText = `
	Help on commands:
	/bet welcome - Details about the chatbet app
	/bet link - Link your account to betfair
	/bet place - Place a bet
	/bet status - Check bet status
	/bet account - Check betfair account status
	` ;
	
	sendTextMessage(recipientId, messageText) ;
}


function listUsers(senderID) {
	var mysql = require('mysql');
	
	
	var connection = mysql.createConnection(config.database) ;
	
	connection.connect();
	
	connection.query('SELECT fname, lname from user_details', function(err, rows, fields) {
			if (!err)
			if (rows.length) {
				for (var i = 0, len = rows.length; i < len; i++) {
					var row = rows[i];
					console.log("Name:" + row.fname + " " + row.lname ) ;
					sendTextMessage(senderID, "Name:" + row.fname + " " + row.lname + "\n" );
				}
			}
			else
				console.log('Error while performing Query.');
	});
	
	connection.end();	
}

function sendLinkMessage(recipientId) {
	
	var messageText = `
	Please click the following link to link your account with betfair.
	https://identitysso.betfair.com/view/vendor-login?client_id=50248&response_type=code&redirect_uri=signup?user_id%3D` + recipientId ;
	
	var messageData = {
		recipient: {
			id: recipientId
		},
		message: {
			text: messageText
		}
	};
	
	callSendAPI(messageData);
}

function sendGenericMessage(recipientId) {
	var messageData = {
		recipient: {
			id: recipientId
		},
		message: {
			attachment: {
				type: "template",
				payload: {
					template_type: "generic",
					elements: [{
							title: "Soccer",
							subtitle: "English Premier League",
							item_url: "",               
							image_url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
							buttons: [{
									type: "postback",
									title: "Liverpool vs Man Utd",
									payload: '{ "type": "event", "home": "Liverpool", "away": "Man Utd" }'
							}, {
								type: "postback",
								title: "Everton vs Man City",
								payload: "Everton vs Man City"
							}, {
								type: "postback",
								title: "Sunderland vs Leicester",
								payload: "Sunderland vs Leicester"
							}],
					}, {
						title: "Soccer",
						subtitle: "English Championship",
						item_url: "",               
						image_url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
						buttons: [{
								type: "web_url",
								url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
								title: "Newcastle vs Brighton"
						}, {
							type: "web_url",
							title: "Leeds vs Notts Forest",
							url: "https://www.oculus.com/en-us/rift/",
						}, {
							type: "web_url",
							title: "Blackburn vs Wolves",
							url: "https://www.oculus.com/en-us/rift/",
						}]
					}]
				}
			}
		}
	};  
	
	callSendAPI(messageData);
}

function sendListMessage(recipientId) {
	var messageData = {
		"recipient": {
			"id": recipientId
		},
		"message": {
			"attachment": {
				"type": "template",
				"payload": {
					"template_type": "list",
					"top_element_style": "compact",
					"elements": [
						{
							"title": "Classic White T-Shirt",
							"image_url": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
							"subtitle": "100% Cotton, 200% Comfortable",
							"default_action": {
								"type": "web_url",
								"url": "https://www.google.co.uk",
								"messenger_extensions": true,
								"webview_height_ratio": "tall",
								"fallback_url": "https://www.google.co.uk"
							},
							"buttons": [
								{
									"title": "Buy",
									"type": "web_url",
									"url": "https://www.google.co.uk",
									"messenger_extensions": true,
									"webview_height_ratio": "tall",
									"fallback_url": "https://www.google.co.uk"
								}
							]
						},
						{
							"title": "Classic Blue T-Shirt",
							"image_url": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
							"subtitle": "100% Cotton, 200% Comfortable",
							"default_action": {
								"type": "web_url",
								"url": "https://www.google.co.uk",
								"messenger_extensions": true,
								"webview_height_ratio": "tall",
								"fallback_url": "https://www.google.co.uk"
							},
							"buttons": [
								{
									"title": "Buy",
									"type": "web_url",
									"url": "https://www.google.co.uk",
									"messenger_extensions": true,
									"webview_height_ratio": "tall",
									"fallback_url": "https://www.google.co.uk"
								}
							]
						},
						{
							"title": "Classic Black T-Shirt",
							"image_url": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
							"subtitle": "100% Cotton, 200% Comfortable",
							"default_action": {
								"type": "web_url",
								"url": "https://www.google.co.uk",
								"messenger_extensions": true,
								"webview_height_ratio": "tall",
								"fallback_url": "https://www.google.co.uk"
							},
							"buttons": [
								{
									"title": "Buy",
									"type": "web_url",
									"url": "https://www.google.co.uk",
									"messenger_extensions": true,
									"webview_height_ratio": "tall",
									"fallback_url": "https://www.google.co.uk"
								}
							]
						},
						{
							"title": "Classic Gray T-Shirt",
							"image_url": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
							"subtitle": "100% Cotton, 200% Comfortable",
							"default_action": {
								"type": "web_url",
								"url": "https://www.google.co.uk",
								"messenger_extensions": true,
								"webview_height_ratio": "tall",
								"fallback_url": "https://www.google.co.uk"
							},
							"buttons": [
								{
									"title": "Buy",
									"type": "web_url",
									"url": "https://www.google.co.uk",
									"messenger_extensions": true,
									"webview_height_ratio": "tall",
									"fallback_url": "https://www.google.co.uk"
								}
							]
						}
					],
					"buttons": [
						{
							"title": "View More",
							"type": "postback",
							"payload": "payload"
						}
					]
				}
			}
		}
	};
	
	callSendAPI(messageData);
	
}

function sendWebViewMessage(recipientId) {
	var messageData = {
		recipient: {
			id: recipientId
		},
		message: {
			attachment: {
				type: "template",
				payload: {
					template_type: "generic",
					elements: [{
							title: "Soccer",
							subtitle: "English Premier League",
							item_url: "",               
							image_url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
							"buttons":[
								{
									"type":"web_url",
									"url":"https://www.google.co.uk",
									"title":"Soccer",
									"webview_height_ratio": "compact"
								},
								{
									"type":"web_url",
									"url":"https://www.google.co.uk",
									"title":"Horse Racing",
									"webview_height_ratio": "compact"
								},
								{
									"type":"web_url",
									"url":"https://www.google.co.uk",
									"title":"Golf",
									"webview_height_ratio": "compact"
								}
							],
					}, {
						title: "Soccer",
						subtitle: "English Championship",
						item_url: "",               
						image_url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
						buttons: [{
								type: "web_url",
								url: "http://www.jackpot.co.uk/wp-content/uploads/betfair-advert.jpg",
								title: "Newcastle vs Brighton"
						}, {
							type: "web_url",
							title: "Leeds vs Notts Forest",
							url: "https://www.oculus.com/en-us/rift/",
						}, {
							type: "web_url",
							title: "Blackburn vs Wolves",
							url: "https://www.oculus.com/en-us/rift/",
						}]
					}]
				}
			}
		}
	};  
	
	callSendAPI(messageData);
}

function callSendAPI(messageData) {
	request({
			uri: 'https://graph.facebook.com/v2.6/me/messages',
			qs: { access_token: process.env.PAGE_ACCESS_TOKEN },
			method: 'POST',
			json: messageData
			
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var recipientId = body.recipient_id;
			var messageId = body.message_id;
			
			console.log("Successfully sent generic message with id %s to recipient %s", 
				messageId, recipientId);
		} else {
			console.error("Unable to send message.");
			console.error(response);
			console.error(error);
		}
	});  
}

function getAllEventTypes(senderID, options) {
	console.log("Get event types");
	// Retrieve event id from previous response
	//var eventId = retrieveEventId(response);
	
	
	var str = '';
	var requestFilters = '{"filter":{}}';
	var jsonRequest = constructJsonRpcRequest('listEventTypes', requestFilters );
	
	var req = https.request(options,function (res){
            res.setEncoding(DEFAULT_ENCODING);
            res.on('data', function (chunk) {
            		str += chunk;
            });
            
            res.on('end', function (chunk) {
            		var response = JSON.parse(str);
            		handleError(response);
            		console.log("Response:" + response ) ;
            		console.log("PRETTY:" + JSON.stringify(response, null, 2) ) ;
            		
            		for (var i = 0; i<= Object.keys(response.result).length - 1; i++ ) {
            			console.log("result:" + response.result[i].eventType.name + " i=" + i + " Total:" + Object.keys(response.result).length) ;
            			sendTextMessage(senderID, response.result[i].eventType.name + "(" + response.result[i].eventType.id + ")\n" );
            		    // Display top 5 only
            			if ( i == 4 ) { break } ;
            		}
            		
            });
    });
    req.write(jsonRequest, DEFAULT_ENCODING);
    req.end();
    req.on('error', function(e) {
            console.log('Problem with request: ' + e.message);
    });
}

function findHorseRaceId(options) {
	console.log("Get horse racing event id");
	// Define Horse Racing in filter object
	var requestFilters = '{"filter":{}}';
	var jsonRequest = constructJsonRpcRequest('listEventTypes', requestFilters );
	var str = '';
	var req = https.request(options,function (res){
            res.setEncoding(DEFAULT_ENCODING);
            res.on('data', function (chunk) {
            		str += chunk;
            });
            
            res.on('end', function (chunk) {
            		// On resposne parse Json and check for errors
            		var response = JSON.parse(str);
            		handleError(response);
            		// Retrieve id from response and get next available horse race
            		getNextAvailableHorseRace(options, response);
            });
            
    });
    // Send Json request object
    req.write(jsonRequest, DEFAULT_ENCODING);
    req.end();
    
    req.on('error', function(e) {
            console.log('Problem with request: ' + e.message);
    }); 
}

// Get next horse race based on current date
function getNextAvailableHorseRace(options, response) {
	// Retrieve event id from previous response
	var eventId = retrieveEventId(response);
	var jsonDate = new Date().toJSON();
	console.log("Get next available horse race starting from date: " + jsonDate);
	var str = '';
	var requestFilters = '{"filter":{"eventTypeIds": [' + eventId + '],"marketCountries":["GB"],"marketTypeCodes":["WIN"],"marketStartTime":{"from":"'+jsonDate+'"}},"sort":"FIRST_TO_START","maxResults":"1","marketProjection":["RUNNER_DESCRIPTION"]}';
	var jsonRequest = constructJsonRpcRequest('listMarketCatalogue', requestFilters );
	
	var req = https.request(options,function (res){
            res.setEncoding(DEFAULT_ENCODING);
            res.on('data', function (chunk) {
            		str += chunk;
            });
            
            res.on('end', function (chunk) {
            		var response = JSON.parse(str);
            		handleError(response);
            		// Get list of runners that are available in that race
            		getListOfRunners(options, response);
            });
    });
    req.write(jsonRequest, DEFAULT_ENCODING);
    req.end();
    req.on('error', function(e) {
            console.log('Problem with request: ' + e.message);
    });
}

function getListOfRunners(options, response) {
	var marketId = retrieveMarketId(response);
	console.log("Get list of runners for market Id: " + marketId);
	var requestFilters = '{"marketIds":["' + marketId + '"],"priceProjection":{"priceData":["EX_BEST_OFFERS"],"exBestOfferOverRides":{"bestPricesDepth":2,"rollupModel":"STAKE","rollupLimit":20},"virtualise":false,"rolloverStakes":false},"orderProjection":"ALL","matchProjection":"ROLLED_UP_BY_PRICE"}';
	var jsonRequest = constructJsonRpcRequest('listMarketBook', requestFilters );
	var str = '';
	var req = https.request(options,function (res){
            res.setEncoding(DEFAULT_ENCODING);
            res.on('data', function (chunk) {
            		str += chunk;
            });
            
            res.on('end', function (chunk) {
            		var response = JSON.parse(str);
            		handleError(response);
            		// Place bet on first runner
            		placeBet(options, response, marketId);
            });
    });
    req.write(jsonRequest, DEFAULT_ENCODING);
    req.end();
    req.on('error', function(e) {
            console.log('Problem with request: ' + e.message);
            return;
    });
}

function placeBet(options, response, marketId) {
	var str = '';
	var selectionId  = retrieveSelectionId(response);
	// Invalid price and size, change that to minimum price of 2.0
	var price = '2';
	var size = '0.01';
	var customerRef = new Date().getMilliseconds();
	console.log("Place bet on runner with selection Id: " + selectionId);
	var requestFilters = '{"marketId":"'+ marketId+'","instructions":[{"selectionId":"' + selectionId + '","handicap":"0","side":"BACK","orderType":"LIMIT","limitOrder":{"size":"' + size + '","price":"' + price + '","persistenceType":"LAPSE"}}],"customerRef":"'+customerRef+'"}';
	var jsonRequest = constructJsonRpcRequest('placeOrders', requestFilters );
	var req = https.request(options,function (res){
            res.setEncoding(DEFAULT_ENCODING);
            res.on('data', function (chunk) {
            		str += chunk;
            });
            res.on('end', function (chunk) {
            		var response = JSON.parse(str);
            		handleError(response);
            		console.log(JSON.stringify(response, null, DEFAULT_JSON_FORMAT));
            });
    });
    req.write(jsonRequest, DEFAULT_ENCODING);
    req.end();
    req.on('error', function(e) {
            console.log('Problem with request: ' + e.message);
    });
}

// get event id from the response
function retrieveEventId(response) {
	for (var i = 0; i<= Object.keys(response.result).length; i++ ) {
		if (response.result[i].eventType.name == 'Horse Racing'){
			return response.result[i].eventType.id;
		}
	}
}

// get selection id from the response
function retrieveSelectionId(response) {
	return response.result[FIRST_INDEX].runners[FIRST_INDEX].selectionId;
}

// get market id from the response
function retrieveMarketId(response) {
	return response.result[FIRST_INDEX].marketId;
}


function constructJsonRpcRequest(operation, params) {
	return '{"jsonrpc":"2.0","method":"SportsAPING/v1.0/' +  operation + '", "params": ' + params + ', "id": 1}';
}

// Handle Api-NG errors, exception details are wrapped within response object 
function handleError(response) {
	// check for errors in response body, we can't check for status code as jsonrpc returns always 200
	if (response.error != null) {
		// if error in response contains only two fields it means that there is no detailed message of exception thrown from API-NG
		if (Object.keys(response.error).length > 2) {
			console.log("Error with request!!");
			console.log(JSON.stringify(response, null, DEFAULT_JSON_FORMAT));
			console.log("Exception Details: ");
			console.log(JSON.stringify(retrieveExceptionDetails(response), null, DEFAULT_JSON_FORMAT));
		}
		process.exit(1);
	}
}

// Get exception message out of a response object
function retrieveExceptionDetails(response) {
	return response.error.data.APINGException;
}

// Set Express to listen out for HTTP requests
var server = app.listen(process.env.PORT || 3443, function () {
		console.log("Listening on port %s", server.address().port);
		console.log("ENV: VERIFY_TOKEN %s", process.env.VERIFY_TOKEN) ;
});
